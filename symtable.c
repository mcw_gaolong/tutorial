/***************************************************************************

Copyright (C), 2015 - 2016, Multicoreware Inc.

File name: symtable.c

Author: Long Gao

Version: 1.0     Date: June 25th, 2015

Description: 

   Implement SingleLinkTable‘s operations. Specificly include creating, 

   add, delete, replacing, getting length, getting value according of key 

   and display all the elements of LinkTable.

Others:

Function List:

1. Link_t LinkTable_new(void);
   
   Description: Create LinkTable.

2. void LinkTable_free(Link_t oLink);
   
   Description: Free memory.

3. int LinkTable_getLength(const Link_t oLink);
   
   Description: Get the length of LinkTable.

4. int Link_contains(const Link_t oLink, const char *pcKey);
   
   Description: Judge LinkTable if contains the pcKey.

5. int Link_put(Link_t oLink, const char *pcKey, const void *pvValue);
   
   Description: Put key and value into LinkTable.

6. void *Link_replace(Link_t oLink, const char *pcKey, const void *pvValue);
   
   Description: Replace the value according of key in LinkTable.

7. void *Link_get(const Link_t oLink, const char *pcKey);
   
   Description: Get the value according of key.

8. void *Link_remove(Link_t oLink, const char *pcKey);
   
   Description: Remove element in LinkTable according key.

9. void display(const Link_t head);
   
   Description: Display all elements in LinkTable.

History:

Long Gao 2015/06/24 1.0 build this moudle 

*****************************************************************************/

#include<stdio.h>
#include<stdlib.h>
#include "symtable.h"

/****************************************************************************

Description: Create a new LinkTable and initialize the nodes

Params: void

Return: Link_t or NULL if out of memory 

*****************************************************************************/
Link_t LinkTable_new(void)
{
    Link_t head = NULL;
    Link_t tail = NULL;
    Link_t newNode = NULL;
    char *key = NULL;
    char *value = NULL;

    head = (Link_t)malloc(sizeof(struct node));
    if(head == NULL)
    {
	printf("Alloc memory failed!\n");
        return NULL;
    }
    key = "001";
    value = "val001";
    head->key = key;
    head->value = value;
    head->next = NULL;
    tail = head;

    Link_put(head, "002", "val002"); 
    Link_put(head, "003", "val003");
    Link_put(head, "004", "val004");
    Link_put(head, "005", "val005");
    Link_put(head, "006", "val006");

    return head;
}

/****************************************************************************

Description: Free memory

Params: Link_t

Return: void

*****************************************************************************/
void LinkTable_free(Link_t oLink)
{
    if(oLink == NULL)
    {
        return;
    }
    Link_t p = NULL;
    Link_t q = NULL;
    p = oLink;
    while(p != NULL)
    {
	q = p;
	p = q->next;
        //free(q->key);
        //free(q->value);
 	free(q);
        q = NULL;
    }
}

/****************************************************************************

Description: Get the length of LinkTable

Params: Link_t

Return: The length of LinkTalbe

*****************************************************************************/
int LinkTable_getLength(const Link_t oLink)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist !\n");
        return 0;
    }

    int count = 0;
    Link_t p = NULL;
    for(p = oLink; p != NULL; p = p->next)
    {
    	count = count + 1;
    }
    return count;
}

/****************************************************************************

Description: Judge LinkTable if contains the key

Params: Link_t, key

Return: 1 if exist else return 0

*****************************************************************************/
int Link_contains(const Link_t oLink, const char *pcKey)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist!\n");
        return 0;
    }
    if(pcKey == NULL)
    {
        return 0;
    }

    Link_t p = NULL;
    
    if(strcmp(pcKey, oLink->key) == 0)
    {
        return 1;
    }
    for(p = oLink->next; p != NULL; p = p->next)
    {
        if(strcmp(pcKey,p->key) == 0)
        {
            return 1;
        }
    }
    return 0;
}

/****************************************************************************

Description: Put the new node into an existing LinkTable

Params: Link_t, key, value

Return: 1 if not contains key and don't have the same value, else return 0

*****************************************************************************/
int Link_put(Link_t oLink, const char *pcKey, const void *pvValue)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist PPP!\n");
        return 0;
    }
    if(pcKey == NULL || pvValue == NULL)
    {
        return 0;
    }
    Link_t pv = NULL;
    Link_t pTail = NULL;
    for(pv = oLink; pv != NULL; pv = pv->next)
    {  
        //printf("The  value is:===%s\n", pv->value);
        pTail = pv;
        /*if(strcmp(pvValue,pv->value) == 0)
        {
            printf("The value is existed!\n");
            return 0;
        }*/
    }
    if(Link_contains(oLink, pcKey) == 0)
    {
        //printf("The pcKey is:===%s\n", pcKey);
        Link_t newNode = (Link_t)malloc(sizeof(struct node));
        if(newNode == NULL)
        {
            printf("Alloc memory failed!\n");
            return 0;
        }
        newNode->key = (char*)pcKey;
        newNode->value = (char*)pvValue;
        newNode->next = NULL;
        //printf("pv->key:%s\n", pTail->key);
        pTail->next = newNode;
        return 1;
    }
    return 0;
}

/****************************************************************************

Description: Replace value with the new one according of the key

Params: Link_t, key, value

Return: Link_t if it contains key and replace the new value, else return NULL pointer

*****************************************************************************/
void *Link_replace(const Link_t oLink, const char *pcKey, const void *pvValue)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist !\n");
        return;
    }
    if(pcKey == NULL || pvValue == NULL)
    {
        return;
    }

    Link_t p = NULL;
    Link_t pRes = NULL;
    for(p = oLink; p != NULL; p = p->next)
    {
        if(strcmp(pcKey, p->key) == 0)
        {
            pRes = p;
            p->value = (char*)pvValue;
            return;
        }
    }
    return pRes;
}

/****************************************************************************

Description: Get value according key

Params: Link_t, key

Return: Value if it contains key, else return NULL pointer

*****************************************************************************/
void *Link_get(const Link_t oLink, const char *pcKey)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist !\n");
        return NULL;
    }
    if(pcKey == NULL)
    {
        return NULL;
    }

    Link_t p = NULL;
    for(p = oLink; p != NULL; p = p->next)
    {
        if(strcmp(p->key,pcKey) == 0)
        {
            return p->value;
        }
    }
    return NULL;
}

/****************************************************************************

Description: Remove the node according key

Params: Link_t, key

Return: Link_t if it contains key, else return NULL pointer

*****************************************************************************/
void *Link_remove(Link_t oLink, const char *pcKey)
{
    if(oLink == NULL)
    {
        printf("The link isn't exist !\n");
        return NULL;
    }
    if(pcKey == NULL)
    {
        return NULL;
    }

    Link_t pre = oLink;
    Link_t p = oLink->next;

    if(strcmp(pre->key,pcKey) == 0)
    {
        oLink = p;
        if(oLink != NULL)
        {
            display(oLink);
        }
        return pre;
    }

    while(p != NULL)
    {
        if(strcmp(p->key, pcKey) == 0)
        {
            pre->next = p->next;
            if(oLink != NULL)
            {
                display(oLink);
            }
            return p;
        }
        pre = p;
        p = p->next;
    }
    return NULL;
}

/****************************************************************************

Description: Display all nodes of LinkTable

Params: Link_t

Return: void

*****************************************************************************/
void display(const Link_t head)
{
    Link_t p = NULL;
    printf("      KEY        VALUE\n");
    for(p = head; p != NULL; p = p->next){
    	printf("      %s        %s\n", p->key, (char*)p->value);
    }
}
