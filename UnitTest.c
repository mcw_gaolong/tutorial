
#include <stdio.h>
#include <stdlib.h>
#include "symtable.h"

int main()
{
    printf("\n------------------------------ Begin Unit Test ----------------------------------\n");

    Link_t head;

    //Test LinkTable_new
    printf("\n******************************* Test_1 ******************************************\n");
    printf("\nFunction Link_t LinkTable_new(void), initialize 6 elements:\n");
    head = LinkTable_new();
    display(head);
    
    //Test LinkTable_getLength
    printf("\n******************************* Test_2 ******************************************\n");
    printf("\nFunction int LinkTable_getLength(const Link_t oLink):\n");
    int length = LinkTable_getLength(head);
    printf("      LENGTH\n");
    printf("      %d\n",length);

    //Test Link_contains
    printf("\n******************************* Test_3 ******************************************\n");
    printf("\nFunction int Link_contains(const Link_t oLink, const char *pcKey):\n");
    if(Link_contains(head, "002") == 1)
    {
        printf("     \"002\" EXISTED!\n");
    }
    else
    {
        printf("     \"002\" NOT EXIST!\n");
    }
    if(Link_contains(head, "008") == 1)
    {
        printf("     \"008\" EXISTED!\n");
    }
    else
    {
        printf("     \"008\" NOT EXIST!\n");
    }

    //Test Link_put
    printf("\n******************************* Test_4 ******************************************\n");
    printf("\nFunction int Link_put(Link_t oLink, const char *pcKey, const void *pvValue)\nPut key:\"010\" value:\"val010\":\n");
    Link_put(head, "010", "val010");
    display(head);
    

    //Test Link_get
    printf("\n******************************* Test_5 ******************************************\n");
    printf("\nFunction void *Link_get(const Link_t oLink, const char *pcKey):\n");
    char *val005 = (char*)Link_get(head, "005");
    printf("      KEY 005\'S VALUE      %s\n", val005);
    char *val333 = (char*)Link_get(head, "333");
    printf("      KEY 333\'S VALUE      %s\n", val333);

    //Test Link_replace
    printf("\n******************************* Test_6 ******************************************\n");
    printf("\nFunction void *Link_replace(Link_t oLink, const char *pcKey, const void *pvValue)\nReplace key:\"002\" value:\"rep002\":\n");
    Link_replace(head, "002", "rep002");
    display(head);

    //Test Link_remove
    printf("\n******************************* Test_7 ******************************************\n");
    printf("\nFunction void *Link_remove(Link_t oLink, const char *pcKey)Remove key:\"003\":\n");
    Link_t tmp = Link_remove(head, "003");
    Link_t tmp2 = Link_remove(head, "003");
    if(tmp2 == NULL)
    {
        printf("\nREMOVE \"003\" AGAIN, RETURN NULL!\n");
    }
    if(head == tmp)
    {
        LinkTable_free(head);
    } 
    else
    {
        free(tmp);
        LinkTable_free(head);
    }

    printf("\n------------------------------ End Unit Test ------------------------------------\n\n");

    return 1;
}

