/***************************************************************************

Copyright (C), 2015 - 2016, Multicoreware Inc.

File name: symtable.c

Author: Long Gao

Version: 1.0     Date: June 25th, 2015

Description: 

   Implement SingleLinkTable‘s operations. Specificly include creating, 

   add, delete, replacing, getting length, getting value according of key 

   and display all the elements of LinkTable.

Others:

Function List:

1. Link_t LinkTable_new(void);
   
   Description: Create LinkTable.

2. void LinkTable_free(Link_t oLink);
   
   Description: Free memory.

3. int LinkTable_getLength(const Link_t oLink);
   
   Description: Get the length of LinkTable.

4. int Link_contains(const Link_t oLink, const char *pcKey);
   
   Description: Judge LinkTable if contains the pcKey.

5. int Link_put(Link_t oLink, const char *pcKey, const void *pvValue);
   
   Description: Put key and value into LinkTable.

6. void *Link_replace(Link_t oLink, const char *pcKey, const void *pvValue);
   
   Description: Replace the value according of key in LinkTable.

7. void *Link_get(const Link_t oLink, const char *pcKey);
   
   Description: Get the value according of key.

8. void *Link_remove(Link_t oLink, const char *pcKey);
   
   Description: Remove element in LinkTable according key.

9. void display(const Link_t head);
   
   Description: Display all elements in LinkTable.

History:

Long Gao 2015/06/24 1.0 build this moudle 

*****************************************************************************/

#ifndef SLT_H
#define SLT_H

/****************************************************************************

Description: The structure of Link Table. Contain three fields.

key : A char* pointer.

value : A void* pointer.

next : A struct node pointer which point to another node.

*****************************************************************************/
struct node
{
    char *key;
    void *value;
    struct node *next;
};

/****************************************************************************

Description: A pointer of struct node which defined by typedef

*****************************************************************************/
typedef struct node* Link_t;

/****************************************************************************

Description: Create a new LinkTable and initialize the nodes

Params: void

Return: Link_t or NULL if out of memory 

*****************************************************************************/
Link_t LinkTable_new(void);

/****************************************************************************

Description: Free memory

Params: Link_t

Return: void

*****************************************************************************/
void LinkTable_free(Link_t oLink);

/****************************************************************************

Description: Get the length of LinkTable

Params: Link_t

Return: The length of LinkTalbe

*****************************************************************************/
int LinkTable_getLength(const Link_t oLink);

/****************************************************************************

Description: Judge LinkTable if contains the key

Params: Link_t, key

Return: 1 if exist else return 0

*****************************************************************************/
int Link_contains(const Link_t oLink, const char *pcKey);

/****************************************************************************

Description: Put the new node into an existing LinkTable

Params: Link_t, key, value

Return: 1 if not contains key and don't have the same value, else return 0

*****************************************************************************/
int Link_put(Link_t oLink, const char *pcKey, const void *pvValue);

/****************************************************************************

Description: Replace value with the new one according of the key

Params: Link_t, key, value

Return: Link_t if it contains key and replace the new value, else return NULL pointer

*****************************************************************************/
void *Link_replace(const Link_t oLink, const char *pcKey, const void *pvValue);


/****************************************************************************

Description: Get value according key

Params: Link_t, key

Return: Value if it contains key, else return NULL pointer

*****************************************************************************/
void *Link_get(const Link_t oLink, const char *pcKey);

/****************************************************************************

Description: Remove the node according key

Params: Link_t, key

Return: Link_t if it contains key, else return NULL pointer

*****************************************************************************/
void *Link_remove(Link_t oLink, const char *pcKey);

/****************************************************************************

Description: Display all nodes of LinkTable

Params: Link_t

Return: void

*****************************************************************************/
void display(const Link_t head);

#endif

